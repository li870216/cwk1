// Class for COMP2931 Coursework 1

package comp2931.cwk1;
import java.util.Calendar;

/**
 * Simple representation of a date
 */
public class Date {


  //Class constants
  private static final int MONTH_PER_YEAR = 12;
  private static final int DAY_PER_YEAR = 365;

  private static final int[] MONTH_LENGTH = {31,28,31,30,31,30,31,31,30,31,30,31};
  private static final int[] LEAP_YEAR_MONTH_LENGTH = {31,29,31,30,31,30,31,31,30,31,30,31};


  // Instance variables
  private int year;
  private int month;
  private int day;

  /**
   * Creates a Time object representing the current time.
   */
  public Date() {
    Calendar now = Calendar.getInstance();
    year = now.get(Calendar.YEAR);
    month = now.get(Calendar.MONTH) + 1;
    day = now.get(Calendar.DATE);
  }


  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;
    set (y,m,d);
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Returns the day of year
   *
   * @return DayOfYear
   */
  public int getDayOfYear() {
    int d = 0;
    int c = 0;
    int in,index;
    if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {

      for (in = 0; in != month -1; in++) {
        d+= LEAP_YEAR_MONTH_LENGTH[in];
      }
      return d+day;
    }

    else {
      for(index=0; index != month-1; index++){
        c += MONTH_LENGTH[index];
      }
      return c+day;
    }
  }


  /**
   * The Date object is used to representing a user-specific date
   *
   * @param dateString string containing years, months and days
   * @throws IllegalArgumentException if years, months or days are invalid
   */
  public Date(String dateString) {
    String[] parts = dateString.split("-");
    if (parts.length < 2 || parts.length > 3) {
      throw new IllegalArgumentException("ill-formed date string");
    }

    int y = Integer.parseInt(parts[0]);
    int m = Integer.parseInt(parts[1]);
    int d = Integer.parseInt(parts[2]);

    set(y, m, d);
  }

  // Helper method to assign values to instance variables
  // (not part of public interface)
  private void set(int y, int m, int d) {

    if (y < 0 || y > 9999) {
      throw new IllegalArgumentException("years out of range");
    }
    else if (m < 0 || m > MONTH_PER_YEAR) {
      throw new IllegalArgumentException("months out of range");
    }

    else {
      year = y;
      month = m;
    }

    if((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)){
      if (d<0 || d>LEAP_YEAR_MONTH_LENGTH[month-1] ){
        throw new IllegalArgumentException("days out of range");
      }
    }

    else {
      if (d<0 || d>MONTH_LENGTH[month-1] ){
        throw new IllegalArgumentException("days out of range");
      }
    }

    day = d;

    }

  /**
   * Tests whether this Date is equal to some other object.
   *
   * @param tmp Object with which this Date is to be compared
   * @return True if the two objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object tmp){
    boolean s = false;

    if(tmp == this){
      s = true;
    }
    else if (tmp != null && tmp instanceof Date){
      final Date n = (Date) tmp ;
      if(getYear() == n.getYear()
          && getMonth() == n.getMonth()
          && getDay() == n.getDay()){
        s = true;
      }
    }
    return s;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

}
