package comp2931.cwk1;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DateTest {



  @Test
  public void dateToString() {
    Date String = new Date(2012, 12, 12);
    assertEquals("Date should be in string 'year-month-day'", "2012-12-12", String.toString());
  }

  @Test
  public void year() {
    Date y1 = new Date(1997, 11, 18);
    assertThat(y1.getYear(), is(1997));
  }

  @Test(expected = IllegalArgumentException.class)
  public void yearsTooLow() {
    new Date(-1, 11, 12);
  }

  @Test(expected = IllegalArgumentException.class)
  public void yearsTooHigh() {
    new Date(10000, 12, 12);
  }


  @Test
  public void month() {
    Date m1 = new Date(1996, 11, 03);
    assertThat(m1.getMonth(), is(11));
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthsTooLow() {
    new Date(2011, -10, 13);
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthTooHigh() {
    new Date(2011, 13, 03);
  }


  @Test
  public void day() {
    Date m1 = new Date(1996, 11, 03);
    assertThat(m1.getDay(), is(03));
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooLow() {
    new Date(2001, 06, -1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooHighInSolarMonth() {
    new Date(2001, 06, 32);
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooHighInLunarMonth() {
    new Date(2001, 06, 31);
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooHighInFeb() {
    new Date(2001, 02, 29);
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooHighInLeapYearFeb() {
    new Date(2004, 02, 30);
  }


  @Test
  public void equality() {
    Date ex = new Date(2017, 12, 12);

    assertTrue(ex.equals(ex));
    assertTrue(ex.equals(new Date(2017, 12, 12)));
    assertFalse(ex.equals(new Date(2018, 12, 12)));
    assertFalse(ex.equals(new Date(2011, 11, 12)));
    assertFalse(ex.equals(new Date(2018, 12, 05)));
    assertFalse(ex.equals("2017-12-12"));
  }

  @Test
  public void getDayOfYear() {
    Date ex1 = new Date(2000, 12, 31);
    Date ex2 = new Date(2010, 12, 31);

    assertThat(ex1.getDayOfYear(), is(366));
    assertThat(ex2.getDayOfYear(), is(365));
  }

}